
# Introduction à Python pour les débutants

# 1. Syntaxe de base

## Variables
Python utilise des variables pour stocker des données.
```python
age = 25
nom = "Alice"
```
C'est variable peuvent avoir plusieurs types
- Entiers (int) : Représentent des nombres entiers, par exemple, 1, 42, -10.

- Nombres à virgule flottante (float) : Représentent des nombres décimaux, par exemple, 3.14, -0.5, 2.0.

- Chaînes de caractères (str) : Représentent des séquences de caractères, par exemple, "Hello, World!", 'Python

## Opérations mathématiques

Python permet de réaliser des opérations mathématiques de base.

```python
a = 10
b = 5
somme = a + b
produit = a * b
```
Il y a plusieurs opérations

??? note  "Opérateurs arithmétiques"

    - Addition (+)
    - Soustraction (-)
    - Multiplication (*)
    - Division (/)
    - Division entière (//)
    - Modulo (%)
    - Exponentiation (**)
??? note   "Opérateurs de comparaison (retournent généralement   True ou False) :"

    - Égalité (==)
    - Inégalité (!=)
    - Supérieur à (>)
    - Inférieur à (<)
    - Supérieur ou égal à (>=)
    - Inférieur ou égal à (<=)
??? note "Opérateurs logiques (pour les expressions booléennes):" 

    - ET logique (and)
    - OU logique (or)
    - NON logique (not)
??? note  "Opérateurs d'affectation :"

    - Affectation simple (=)
    - Affectation avec opération (+=, -=, *=, /=, //=, %=, **=)
??? note " Opérateurs d'identité (comparaison d'objets) :"

    - Est identique à (is)
    - N'est pas identique à (is not)
??? note " Opérateurs de membre (pour les structures de données comme les listes, les tuples, les ensembles, etc.):"
    - Appartient à (in)
    - N'appartient pas à (not in)
## Chaînes de caractères
Les chaînes de caractères sont des séquences de caractères.
```python
message = "Bonjour, Python !"
longueur = len(message)
```
# 2. Structures de contrôle
## Boucles
Les boucles permettent de répéter des actions.
Il y a 2 types de boucles

1. Les boucles ``for`` sont des boucles où on connaît la fin
    ```python
    for i in range(5):
        print("Répétition", i)
    ```
2. Les boucles ``` while ``` sont des boucles a  fins indéterminées .

    ```python
    while condition:
        print("Tant que la condition est vraie")
    ```
    Elle se termine quand la condition est atteinte .
## Instructions conditionnelles
Les instructions conditionnelles permettent d'exécuter du code en fonction de conditions.

```python
    age = 18
if age < 18:
    print("Vous êtes mineur.")
else:
    print("Vous êtes majeur.")

 ```

# 3. Fonctions
Les fonctions sont des blocs de code réutilisables.

## Définir une fonction
```python
 def dire_bonjour(nom):
    print("Bonjour, " + nom + " !")

 dire_bonjour("Alice")

 ```
## Arguments et valeurs de retour
Les fonctions peuvent prendre des arguments et retourner des valeurs.
```python
    def additionner(a, b):
        somme = a + b
        return somme

    resultat = additionner(10, 5)
 ```
