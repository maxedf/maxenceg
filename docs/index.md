---
author: Votre nom
title: "🏡 Accueil"
---

# 🏡 Site de base à modifier

Ce site est un modèle de site avec Python. Vous pouvez l'explorer et modifier les fichiers à votre guise.

Après chaque mise à jour de fichier, le site sera construit automatiquement.

